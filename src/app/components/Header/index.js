import React from 'react'
import Image from 'next/image'
const img = require('../../assets/logo_uget_black.png')

const menuItens = [
   {
      name: 'Home',
      redict: '/'
   },
   {
      name: 'Soluções',
      redict: '/'
   },
   {
      name: 'Clientes',
      redict: '/'
   },
   {
      name: 'Quem Somos',
      redict: '/'
   }
]
const Header = () => {
   return (
      <div className='flex py-4 bg-white px-10 justify-around' >
         <Image src={img} height={25.67} width={100} />
         <div className='flex gap-4'>
            {menuItens.map((item) =>
               <p className='text-black'>{item.name}</p>
            )}
         </div>
      </div>
   )
}

export default Header